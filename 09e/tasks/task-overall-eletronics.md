---
title: Overall Eletronics
date: 14/12/2018
---

# Introduction

The task aims to encourage the recruits to have a better understanding of the 
eletronics used in a formula student car, more specifically in a car built by
FST Lisboa.

# Topics to Explore

## Powertrain - From the Battery to the Wheel 

+ This should include the topics that follow as well as their interaction with each other and the overall system of the car:
    - Battery (Basic operation and Overall Role in the car including BMS architecture);
    - Inverters (Basic operation Fundamentals and Overall Role in the car);
    - Motors (Basic operation and Overall Role in the car);
    - Gearing (Basic operation Fundamentals)
    - Wheel 

## Pedal Signal Path

+ The following topics must be explored and explained:
    - Sensor used to acquire the pilots input (by means of the Torque Encoder);
    - Working Principals of the Pedal (interaction with the inverters).

## Safety/Indication Measures

+ The following topics must be explored and explained:
    - Shutdown Circuit and AIR (Working Fundamentals and Definitions)
    - Master Switches (identify and explain the role of each one of them)
    - BSPD (Basic opertion)
    - TSAL (Basic operation)
    - Brake Light (Basic operation)
    - RTDS (Basic operation)
    - IMD (Basic operation)
    - BOTS (Basic operation)
    - Dash (Describe the Dash; Explain the LEDs present and their purpose)
    - HVD (Basic operation)
	- Precharge and discharge systems

## Sensors

+ The following topics should be explored and explained:
    - AHRS + GPS (Purpose and Bbsic operation);
	- Linear potentiometers (Purpose);
    - Strain Gauges (Purpose and basic operation);

## Telemetry

+ The following topics should be explored and explained:
    - Purpose;
    - Basic operation;

## Interface

+ The following topics should be explored and explained:
    - Purpose;
    - Basic Operation;
    
## Harnessing

+ Components of the harness system (connectors and cable);

# Delivery

The recruits must be able to explain all the topics especified above by means
of a small apresentation no longer than 30 minutes. No presentation support material is required.

# Available Documentation

1. A lot information can be found in the - [FS Rules 2019 v1.1 ](https://www.formulastudent.de/fileadmin/user_upload/all/2019/rules/FS-Rules_2019_V1.1.pdf). 
2. There is more information availabe in last years - [ESF](https://drive.google.com/open?id=1KQwbIcim3UNWR7ZXVBzF6J4RvfB0kH3g).
3. Information about race car harnessing - [wiring_ecu](https://www.rbracing-rsr.com/wiring_ecu.html)
4. Information about BMS architecture - [BMS thesis](https://drive.google.com/file/d/1c3QQbKeFOAG_70J_5irNqEpOW5H8jjbd/view?usp=sharing)
5. [Battery Managment System for Formula Student](https://drive.google.com/file/d/18SBEHXpeZZVm9Y0J0-8MTiFKNbmj-Hjj/view?usp=sharing)
5. [Sistemas Eletónicos para o ProjectoFST](https://drive.google.com/file/d/1RfHut4cEPtqBA_V0WZdNguq8JcINyEGT/view?usp=sharing)
6. [Sistema de sensores para Carro de Competição Integrado](https://drive.google.com/file/d/1UwEo_WXrTVjIoMV10CpJAoHwJ0iyLxCV/view?usp=sharing)
7. [Formula Student Racong Champonship of the on-board video aquisition and encoding system](https://drive.google.com/file/d/1b4r16phSq3dT1o-Edjw5FHxXXxouxrec/view?usp=sharing)
8. [Eletronic Engine Control Unit](https://drive.google.com/file/d/1lqEwxV5PQs4vwpX9qzxIjP74r3GnsuOR/view?usp=sharing)
9. [Automotive data aquisition system](https://drive.google.com/file/d/1whp2TTeFKab2jfBfNHNFBzZvLqIXzaPp/view?usp=sharing)
5. Current overall documentation (incomplete) -  [Manual](https://gitlab.com/projectofst/Documentation/manual-09/tree/master/engineering)


Even though, there are tools available online, the best way to truely understand 
how a FS Car works is to talk with members of the team. This way, you will learn more
and begin contact with the team members. 


