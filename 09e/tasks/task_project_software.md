---
title: Project's Software Overview
date: 26/03/2019
---

# Introduction

This task aims to give continuation to the development of the recruitment 
projects every recruit has been working on. 

In the beggining the project was structured and a schedule was created for 
each objective to it associated. 

We have now finished the first major step of the project's development: the 
project objetives are defined, the schematics drawn, and the PCBs on their way 
to production. 

The next step is to start focusing on the code that will be implemented in your
module. 

# Task Description

A document should be created including:

* Objectives
* Structure
* Submitions and Scheduling

## Objectives

What is software usage going to accomplish in your project?

## Structure

For this topic, the recruits shall describe in which situations will a 
microcontroller be useful in the context of the project and how will it be used
(which modules and how will they be implemented for each situation).

## Submitions and Scheduling

This section includes the plan of the work to be realized.

Regular submissions should be scheduled. 


# Delivery

The recruits should take 2 weeks to finish this task.

The task has to be delivered in a pdf format with all the information specified 
in the topics above.

After this task is finished and properly delivered, the recruits should begin 
developing code and testing it so it can be implemented in their project in the 
future.

