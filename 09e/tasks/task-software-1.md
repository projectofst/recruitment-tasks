---
title: Software 1 - Stock Managment for FST Bar
date: 14/12/2018
---

# Introduction

In Covil you can find a bar stocked with drinks, meals and snacks. The bar is
managed by the team which is a hard task since we keep track of the stock
manually.

Your task is to design a web application that automates the bar stock managment.

# Schedule

This task must be completed in 2 weeks.


# Objective

The task objective is to build an application that fullfills its requirements.
It is not intended for you to build a full fledged soluction for stock
managment.

# Requirements

* You must build a web application.
* The stock managment must be done using an sql database.
* The application must be capable of importing purchase lists in csv format
* It must be possible the add or remove items to the stock
* It must be possible to increase or decrease the item count of a particular item
* The items must be identified by the number in the barcode
* The items must have a small description
* The items must have an associated price
* The items must have an associated minimum quantity
* Item text search should be provided
* It should be possible to list the items by price
* It should be possible to list the items by availability
* It should be possible to list all items with availability smaller than the
  minimum quantity
* A mechanism for keeping track of member debt should be implemented
* Debt limits should be implemented

# Implementation

The project should be built as an web app. For the database the usage of sqlite
is mandatory.

We recommend the usage of python and the bottle library for the webserver. 

# References
* [https://www.sqlite.org/index.html](https://www.sqlite.org/index.html)
* [https://bottlepy.org/docs/dev/](https://bottlepy.org/docs/dev/)
* [https://www.python.org/](https://www.python.org/)
 
