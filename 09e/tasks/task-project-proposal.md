---
title: Project Proposal
date: 14/12/2018
---

# Introduction

The recruitment projects are small individual projects, created by FST Lisboa team 
members or sugested by the recruits, with the objective of promoting learning within the recruits as well as 
interaction with team members and other recruits.

The team as came up with a project bank from which the recruits can choose a 
project or draw inspiraction from one.

The duration of this task is two weeks.

# Projects

The projects created establish mostly objectives and general procedures,
so that there is space for the recruits to introduce their ideas and expand their
knowledge through searching and interacting with the team.

The projects are mainly divided into:
* Hardware Driven;
* Software Driven.

The tasks belonging to each subgroup have a similar structure and objectives.

The projects created by FST Lisboa team members for the recruits can be found in
this  [project bank](https://gitlab.com/projectofst/recruitment-tasks/tree/master/projects).

## Hardware
Hardware project should include both eletronics and pcb design as well as 
embedded programming. The projects may not be restricted to these subjects.

# Task

The recruits should use the first week of this task to select a project.

After selecting a task, the recruits will have more or less one week to present 
what they intend to do in order to accomplish the objectives established in the
task description. The proposal has to be well fundamented and will be reviewed
by members of the eletronics area of FST.

# Delivery

## First submission
The recruits must choose a project in under a week.

They should submit the project topic paired with a brief description of the 
project and its objectives.

For this task a pdf must be submited via email. 

## Second submission
After that, they shall make a complete and well fundamented proposal of how they intend
to proceed in order to accomplish the objectives of their task.

This might include:
* Processes;
* MCU used;
* Materials Needed;
* Other Information Considered Relevant.

The recruits will have to present their proposal to one or more members of the team,
who will reviewed it, make suggestions, and clarify existing doubts.

This task should be completed in more or less a week after the project was assigned.

For this task a pdf should be submited via email. After this a discussion should
be scheduled.

After this task, the recruits will proceed to the development stage of the project.

# Project Proposal
The project proposals should include:
 * Brief description
 * Objectives
 * Requirements
 * Submissions and scheduling
 

## Brief description
In this section there should be presented a brief description of the project including which problem is the project trying to solve.

## Objectives

This section must include the qualitative objectives of the project. These 
objectives include the functionality provided by the module.


If the system is an improvement of an existing soluction, what is going to be improved. 

## Requirements
This section must include the design constraints of the project including the 
limits of the functionality provided by the project. 

## Submissions and scheduling
This section includes the plan of the work to be realized.

Regular submissions should be scheduled. The dates for the manufacturing of 
parts should also be scheduled.

# Relevant Link

 [Project Bank](https://gitlab.com/projectofst/recruitment-tasks/tree/master/projects)