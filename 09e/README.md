# Eletro Tasks

## Classes
The relevant classes can be found in the Classes repo.

## Old tasks
Last year tasks can be found in the old-tasks directory. 

## Example projects
Example projects can be found in the example-projects directory

## Overall recruitment structure
[recruitment-fst10e-overall](recruitment-fst10e-overall.md)
