# Recruitment for FST10e 

## Introduction

This file aims to describe the recruitment in the area of eletronics for the car
to be developed in 2019/2020. 

In this file you can find general information concerning the recruitment done in 
this area.

Since the time frame is still not properly stablished, some of these elements 
might need to undergo small alterations. 

## Area's Involvement 

Each member of the area of eletronics that is projecting FST09e should take part 
in the recruitment process for the next car. 

The way the members of the area are relevent will be explained properly later in
this document. 

Generally speaking, the recruitment in this area will envolve the attribution of
individual projects. For these, each project will be supervised by a member of the
current team (of the area of eletronics). Additionally, there will be classes given
to the recruits with the objective of clarifying especific doubts and teaching them 
the fundamentals of what is done in the project (and what will be done during their
recruitment).

## Structure

The recruitment in the team is divided in 3 stages. 

### First Stage

The first stage is the first contact of the recruits with the team. This includes
the recruitment test (about the competition's rules and the team FST Lisboa), the
initial interviews and a presentation about the car organized by João Pinho.

After this stage the recruits will be given the choice to proceed onto an area 
associated with mechanics, eletronics or managment. 

If the recruit's choice is to proceed onto the recruitment in eletronics, he
proceeds to the second stage explicit below, exclusive to eletronics.

### Second Stage

#### Tasks

The beggining of the second stage is the recruits meeting the members of the 
eletronics area. 

The first task presented (to all the recruits who choose this area of interest)
is to prepare a presentation/document explaining the eletrical fundamentals of 
a car like FST09e. This should include powertrain, pedal signal acquisition, 
shutdown circuit, sensors, can interface, can usage in the car, bms architecture, 
generic eletronics module architecture and harnessing in eletric cars. 
Later this task will be better explained and  presented. The recruits will, after 
preparing for a maximum of 2 weeks, present their work to a member of the area 
of eletronics, who should give helpful feedback and clarify and doubts that 
emerged to the recruit.

After this first task, the recruits will be divided in two groups. Some will 
work on software based tasks and others will work with PCBs and with more 
hardware driven tasks (even though these also include the programming of microcontrollers).
As a consequence, we will now have two sub-areas in the recruitment process: 
software; hardware. 
Before the recruits choose which of these areas they prefer it will be explained 
to them the work developed in each one of them.

Now that every recruit is inserted in a sub-area, they will be given access to 
a project bank, created by the team, from where they will choose a project 
from their sub-area to work on. 

The available projects can be found in the [projects](https://gitlab.com/projectofst/recruitment-tasks/tree/freitas-recruitmen-fst010e/projects) folder.


Each recruit will be supervised by a member of the team while developing the project. 

In the sub-area most associated with hardware we have to following objectives (in
chronological order):
+ Drawing the Schematics of the circuit needed for a given project (fundamented
and explained);
+ Use Altium to draw the Schematics;
+ Use Altium to draw the Layout;
+ Programm a Microcontroller.

In the sub-area most associated with software we have to following objectives
+ Database design
+ End user application design
+ Back end server design
+ Front end web application design



#### Classes

In parallel with the first task of the second stage of the recruitment, we will
give 2 classes to the recruits: dsPIC; Altium. 

The first one should explain the general functioning of a microcontroller like 
the ones we use in the car. 

The second class should explain how the Altium software works and give indications
and suggestions about working with it.

Each of these classes will be given by a different member of the eletronics area.

After the first task classes will be continued. In the [classes](https://gitlab.com/projectofst/Documentation/classes)
repository there can be found the topics to be talked about in classes during the
rest of the recruitment as well as when the classes will take place and who will 
give them.


## Third Stage

In the third and last stage, the recruits will be helping with wiring and/or 
soldering, being part of the cars assembly. 

The recruits might also develop projects useful for next years car and/or work on
improving/adapting the project they developed during recruitment's stage two. The
objective of these adaptations would be to create something useful for this years 
and/or next years car.





## Problems (to be eliminated - just an annotation)

We will have to check the time proposed for the duration of recruitment. Problems 
with duration might implicate redimentioning the projects. 

We need more projects to attribute to the recruits. 

We need to assign each member of the eletronics area to a certain number of 
pojects (to supervise and help the recruit developing them).

We need to assign members of the FST09e eletronics team to give classes to the 
recruits through out the semester.

## Note

Projects are not vinculative to FST010e position in the eletronics team. 
Assigment of projects doesn't entitle its assignee to have the project included 
in FST010e.

## Schedule overview
The scheduling of the EL recruitment can be found in [google drive](https://drive.google.com/open?id=1LCYGz6YxbBdaoMKEA4gyXjtLZWH77n_f&authuser=joao.m.freitas@campus.ul.pt).
It contains the planning for 20 weeks of recruitment, 20 classes of  3 hours each one per week.

The planning should be updated along with project development. 
