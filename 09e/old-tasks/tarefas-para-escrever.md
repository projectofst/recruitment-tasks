# Tarefas para escrever

+ Drive dos AIR apartir de condensadores da maneira mais compacta possível.
+ Drive dos AIR de maneira eficiente (economizador).
+ Placa para gerar apenas sinais digitais e analógicos
+ Tarefa de fazer uma BMS para uma célula com prerequisitos de outras tarefas:
    - ADC
    - Relays
    - Medição de corrente
    - As outras tarefas de bateria
+ Tarefa sobre isolamento de sinais (optocouplers, isoladores de comunicações)
+ Tarefa sobre relés
+ Tarefa medição de frequência e de duty cycle de um sinal de PWM externo ao PIC (sinal de PWM do IMD).
+ Tarefa sobre Level Shifting de sinais
+ Discarga
+ Cena para testar o BSPD.
+ Circuito para ligar e desligar DCDC com uma pilha/bateria.