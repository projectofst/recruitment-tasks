# Tarefas de Recrutamento

[1ª Tarefa - Conceitos básicos de um carro formula student elétrico](task-el-g)

[2ª Tarefa - Circuito de plausibilidade do APPS](task-el-1)

[3ª Tarefa - ADCs](task-adc-1)

[4ª Tarefa - PICs Hello World](task-pic-1)

[5ª Tarefa - PICs ADC e PWM](task-pic-2)

## Bateria

[BAT1](task-bat-1)

[BAT2](task-bat-2)

## Motores Eléctricos
[Task List](https://gitlab.com/brunocfigueiredo/RecrutamentoFST/wikis/Home)

## Database (SQL)

[1ª Tarefa - Hello SQL!]

[2ª Tarefa - Database structure diagram]

## Outras tarefas

[IMD1](task-imd-1)

[Devboard 2.0](devboard-2.0)