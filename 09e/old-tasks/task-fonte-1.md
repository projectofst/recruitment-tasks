# Introdução

Antes de mais, **parabéns** por teres acabado todas as tarefas obrigatórias! 
Estás agora pronto/a para começar a fazer algo realmente útil para a equipa.

## O quê

![IMG_0058](/uploads/58bf75f7394a6577307644233bba889c/IMG_0058.JPG)

Queremos substituir a fonte de alimentação da direita, que é a fonte que sempre usámos no projeto, aka big momma, pela da esquerda ( fonte nova que veio da TRACO). Para isso, precisamos de ti. 

## Porquê
A fonte da TRACO permite fazer tudo o que antiga, excepto que não tem indicador de tensão nem de corrente, mas, mais importante, **não tem limitador de corrente**, i.e, sempre que se tentar alimentar algo que esteja em curto circuito, a fonte vai fornecer o seu máximo de corrente (10A). A antiga, por outro lado, permite limitar a corrente que a fonte fornece, para que se possa detectar que algo está em curto circuito sem ser preciso atingir correntes muito altas, não danificando nenhum componente da placa.

É aí que entra o trabalho desta tarefa. Pretendemos que seja possível:
1. **Limitar a corrente da fonte.**
2. Limitar a tensão da fonte.
3. Indicar se a saida da fonte esta limitada pela tensão.
4. Indicar se a saida da fonte esta limitada pela currente.
5. Indicar o valor da tensão à saida da fonte.
6. Indicar o valor da corrente à saida da fonte.
7. Proteger a saida da fonte.

## Como

* Para concretizar os passos 1 e 2, é necessário pesquisar as várias alternativas que existem (alguns exemplos na secção Material, em baixo).

* Para concretizar os passos 5 e 6, é necessário usar um PIC, que adquire os valores de tensão e corrente, e os apresenta em displays de 7 segmentos.

Sendo uma tarefa complexa, será separada em três fases:

* Fase de **design**, em que é devem apresentar tudo o que precisam para conseguir completar a tarefa (Part Numbers de Componentes).

* Fase de implementação, que inclui escrever o código e fazer o circuito necessário.

## Requerimentos

### Tarefas

* [PIC2](task-pic-2)
* [EL1](task-el-1)

### Outros

## Duração programada

Cerca de 14 dias.


## Recursos

### Websites
1. [Fontes de corrente](https://en.wikipedia.org/wiki/Current_source)
2. [Fontes de tensão](https://en.wikipedia.org/wiki/Voltage_source)

### Livros
1. Circuitos com transistores bipolares e MOS, Manuel de Medeiros Silva
2. Analise de Circuitos, A. Brandão Faria


### Documentação

+ [Circuitos Limitadores de Currente](http://www.electronicdesign.com/power/current-limiter-offers-circuit-protection-low-voltage-drop)

### Outros recursos

+ Some pdf or something we have somewhere.
+ Another something in google drive.