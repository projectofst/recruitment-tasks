By now you should be familiar with the FST devboard since you have been using it in most recruitment tasks.

Though it is a helpful piece of hardware there are some flaws in its design. This task is meant to improve on the devboard design to create the 2.0 version.

The devboard was designed to be a small module with plenty of useful functionality. We wanted usb connectivity for easier debugging, support for CAN bus for testing car functions, several power supply rails, all pic33ep256mu806 digital and analog pics available and some indicators and inputs like LEDs and buttons.

## Serial interface
The serial interface is one of the most important features of the FST devboard. The soluction used in the version 1.0 required that the main controller implemented the usb firmware. Unfortunately the USB firmware affects negatively the performance of the rest of the code.
Several alternatives exist, such as the use of a dedicated chip or a dedicated microcontroller.

### Dedicated chip
Soluctions from [FTDI](http://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT260.pdf) and [microchip](http://ww1.microchip.com/downloads/en/DeviceDoc/20005565B.pdf) exist.

A chip based soluction has the following requirements:
* Full speed usb;
* Baudrate matching usb in the interface protocol

### Microcontroller
A microcontroller can be used for the serial interface. That requires writing code for that microcontroller.





