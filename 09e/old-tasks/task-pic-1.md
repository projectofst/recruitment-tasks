# [PIC1] Introdução ao Desenvolvimento com PICs


## Introduction

In the FST08e, almost every PCB packs one of these badboys:
![medium-dsPIC33EP256MU806-TQFP-64](/uploads/8f5fde173f7fc90f609cfec38e73ef23/medium-dsPIC33EP256MU806-TQFP-64.png)

These are PICs, microcontrollers designed by microchip, the particular model we use is the **dsPIC33EP256MU806**.
They are programmable in C, and are used to perform everything that requires digital processing. Think of them as Arduino for men.

This task is aimed at giving you all the necessary competences needed to get started using a PIC, and in the end you'll be able to make an LED blink!

You'll be using a devboard, which is a development board that has one of the previously mentioned PICs, along with all the necessary components to use the PIC, a couple of LEDs and buttons.


## Requirements

### Técnico

Nada

### Tasks

Nada

### Others

Be familiar with the C programming language.

## Programmed Duration

About 7 days.

## Objectives

After completing this task you should:

+ Be familiar with the Microchip PIC environment and the PIC we use.
+ Be familiar with the development board.
+ Be familiar with how we do software development.
+ Have all tools necessary for development for PICs
+ Know how to write a program for a PIC.
+ Know how to program a PIC.

## Description

### Application

You should develop an application which has the following functions:

+ Turn ON an LED constantly.
+ Blink another LED at a frequency of 2 Hz.

#### Tips

Start by configuring everything needed for the PIC to work, supply the PIC and find the pins on the devboard which are connected to LEDs (check the schematic below) 
Note that the oscillator is already configured in our libraries.

Focus on just turning a single LED ON and OFF by hardcoding. When you can you can do this go for the blink or experiment turning ON and OFF multiple LED (There is a RGB LED on the dev board, have fun).

To blink you can use the delay functions or the PIC's timer modules.

## Material

### Websites

+ [dsPIC33EP256MU806 Page](http://www.microchip.com/wwwproducts/en/dsPIC33EP256MU806)(Here you can find all the family references, for the various peripherals, under documentation)

### Books

+ Kernighan, Brian W.; Ritchie, Dennis M (1978). The C Programming Language.

### Documentation
+ [Datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/70616g.pdf)
+ [Programming dsPIC MCUs in C](https://www.mikroe.com/ebooks/programming-dspic-mcus-in-c/introduction)
+ [Unix](https://drive.google.com/open?id=1fhWICEMz_0qSxEP17Y6NDJGwI9WbXLcL)
+ [Make](https://drive.google.com/open?id=14k3rjMbnUJ17C2wrqPZaY1kug5Idi8Um)



### Attachments

**devboard** schematic:

![sch](/uploads/6115479a01d7e2688190ac7034beb2fd/sch.jpg)