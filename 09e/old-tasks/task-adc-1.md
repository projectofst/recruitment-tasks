# [ADC1] ADC basics

## Requirements

### Técnico

None

### Tasks

None

## Programmed Duration

1 week.

## Description

Analog to Digital converters are devices that convert a physical quantity to a given
representation as a digital number. For this investigation the focus should be aimed for
voltage conversion that is by far the most typical type of ADC. The voltage conversion to
a digital number can be done in several different ways each of them with their advantages
and disadvantages, taking that into account different technologies should be used for
different purposes. Doing that selection can somehow be difficult when there is no
clear understanding of the available technologies. As opposite knowing the potentials of
different technologies can improve performance, accuracy and precision of the readings.

## Objectives

#### The work should be divided in the following sections:

Brief Overview of the typical concepts associated with ADC’s with a brief explanation of the following topics (but not limited to):
* Sampling rate
* Resolution 
* Accuracy
* Aliasing 
* Oversampling
* ADC Non-idealities

Brief Overview of different technologies with a brief explanation of their operation principals, the work must necessarily explore the following types of conversion (But not limited to):
* Flash
* SAR

Comparison between different technologies and their advantages

Note that the main objective of this task is for you to have a clear idea of how an ADC works, in a practical manner. 

## Material

**[Google](https://www.google.com)**

### Websites

+ [Analog Devices Tutorials](http://www.analog.com/en/education/search.html?q=*&Filters=resource_type_fac_s:f8eadfaf64cf48afb4ad8b54198f6f2a).
Search for MT-020 to MT-027.

+ [TI Blog post on aliasing](https://e2e.ti.com/blogs_/archives/b/precisionhub/archive/2015/09/04/aliasing-in-adcs-not-all-signals-are-what-they-appear-to-be)

### Delivery Method

+ There are two accepted ways of delivering this task. The preferred method is to make a simple presentation (5-10minutes). The alternative method is to write a short essay.