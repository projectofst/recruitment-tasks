# [BAT2] Bateries

## Requirements

### Técnico

None

### Tasks

[Energy Storage](energy_storage)

## Programmed Duration

A day or two.

## Objectives

After completing this task you should:

+ Know the various types of electrochemical cells, the usual cell and battery formats and their typical applications.
+ Know what are the parameters that define battery cells.
+ Know what parameters must be monitored to guarantee safety of cells. 
+ Know why we use LiCO2 cells.
+ Know how to do simple calculations regarding batteries.
+ Know what happens when you let cells go outside their limits of operation.

## Description

To build a battery you need to understand basic concepts about electrochemical cells (mostly lithium cells). There are multiple manufacturers of this kind of cells and you must be able to read the cell's datasheet and understand if a cell is suitable for your application and which is the best cell for your application.

There are various chemistries of cells which you should know general characteristics.

We use Melasta's LiCO2 cells which provides datasheets like the ones attached (you should understand all parameters on page 3). The datasheet provides cell parameters and care which should be taken when using the cells. You can learn about ao cells work, these parameters and what should be monitored in the cell using a lot of material available on the internet. The website http://batteryuniversity.com/ provides lots of information (which may not be always totally accurate but good enough). The Melasta's datasheet also explains some of this parameters and care that should be taken with the cell.

By understanding the cell parameters you should be able to do some math with the cell parameter so that you can take lots of cells and actually dimension a battery.

You should see some youtube videos about what happens when you let cells/batteries go outside normal operation limits.

You should also study the solution currently used by the team. The best source for this is the ESF.

## Questions

Read the rules and try to comply with them.

### 1 - LV battery.

Say you have the cells with datasheet (1). Dimension a battery pack for the following parameters:

+ Load:
    + 200 W for 2 hour.
    + then 500 W for 30 minutes.
+ Load is powered at 24V by a DC/DC converter with 100% efficiency and a minimum input voltage of 30V.

+ Say what is the battery capacity (Ah), energy content (Wh), min, max and rated voltage, maximum charge and discharge current (A and C) and maximum discharge power at rated voltage.

### 2 - High voltage battery

Say you have cells with datasheet (2). Dimension the cell configuration and segmentation  for a battery pack with the following requirements.

+ A voltage as close as possible to the maximum permissible by rules.
+ A energy of 8 kWh.
+ Able to securely power a load with peaks of 80 kW.

+ Say what is the battery capacity (Ah), energy content (Wh), min, max and rated voltage, maximum charge and discharge current (A and C) and maximum discharge power at rated voltage.

## Material

### Websites

### Books

### Documentation

### Other material
[(1)SLPBA942126_3.7V_6550mAh_10C.pdf](/uploads/20a675eddf1d8cb9f6d243be819fb7cc/SLPBA942126_3.7V_6550mAh_10C.pdf)

[(2)SLPB8763124_7500mAh_15C.pdf](/uploads/1535a97b5cca9f6492b5fe7d736f71d8/SLPB8763124_7500mAh_15C.pdf)