# [PIC2] Desenvolvimento com PICs

## Requerimentos

### Tarefas

[PIC1](task-pic-1)

## Duração programada

Cerca de 7 dias

## Objetivos

Despois de completares a tarefas deves:

+ Ambientares-te com a utilização de git enquanto constróis código.
+ Saber usar e configurar os módulos de timer, ADC e PWM do PIC.
+ Saber controlar o fluxo de um programa que realiza diversas tarefas ao mesmo tempo.

## Descrição

### Programa básico
Na primeira parte da tarefa deverás construir um programa que monitoriza uma tensão, que terá as seguintes funcionalidades:

+ Fazer uma medição de tensão a cada 10 ms utilizando o ADC do PIC.
+ LED Azul - Está ON quando a tensão é menor que 1V durante mais de 1 segundos. Está OFF caso contrário.
+ LED Vermelho - Está ON quando a tensão à entrada do ADC é maior que 2.5V. Está OFF quando a tensão é menor que 2.5V durante mais de 3 segundos.
+ LED Verde - Está ON quando o LED_Vermelho está OFF e a tensão é menor que 2.5V. Está OFF caso contrário.

Antes de começares a programar deverás configurar o teu git. Depois disso precisarás de clonar o template de projeto, criado pela equipa, para o PIC.

O programa deverá ser versionado com Git à medida que é construído de modo a ambientares-te com os comandos básicos de Git (nomeadamente `git init`, `git status`, `git log`, git add, `git commit`). O repositório deverá também ser sincronizado com um repositório _hosted_ no Gitlab (precisarás dos comandos `git clone`, `git remote add`, `git pull`, `git push`).

### Novas funcionalidades

Depois de teres um programa básico irás acrescentar-lhe 3 novas funcionalidades, mas deverás trabalhar em cada uma das funcionalidades de maneira isolada sempre partindo do programa básico. No final quando as 3 funcionalidades estiverem prontas deverás juntá-las no programa final. As funcionalidades são as seguintes.

1. Um segundo canal de monitorização com as mesmas propriedades do primeiro e totalmente independente deste.
2. Se a tensão é superior a 2.5V o LED vermelho deverá ligar e ficar sempre ligado até que se carregue num botão. Mesmo que a tensão baixe de 2.5V.
3. Existe um quarto LED de cor arbitrária cuja intensidade deverá ser regulada proporcionalmente à tensão medida. Para isto deverás usar PWM que é implementado nos nossos PIC com o módulo de Output Compare e não com o módulo de High Speed PWM.

O teu programa básico deverá ter uma série de commits no branch _master_. Para cada uma das funcionalidades deverás criar um novo branch que parte do último commit do teu programa básico. Deverás desenvolver cada uma das funcionalidades no seu branch próprio e no final, quando as três funcionalidades estiverem implementadas deverás fazer merge das três funcionalidades no branch _master_. Depois do merge o programa deverá conter todas as funcionalidades pedidas a funcionar em harmonia.

## Recursos

Ir à sala de eletro onde está todo o material preciso e pessoas que podem ajudar.

### Websites

* [Gitlab da equipa](https://gitlab.com/projectofst)
* [How to ADC](https://gitlab.com/projectofst/Documentation/EletroDesign/wikis/Introdu%C3%A7%C3%A3o-ao-m%C3%B3dulo-de-ADC)

### Livros

+ Kernighan, Brian W.; Ritchie, Dennis M (1978). The C Programming Language.

### Documentação
+ [Unix](https://drive.google.com/open?id=1fhWICEMz_0qSxEP17Y6NDJGwI9WbXLcL)
+ [Make](https://drive.google.com/open?id=14k3rjMbnUJ17C2wrqPZaY1kug5Idi8Um)