# [IMD1] Leitura do sinal de PWM do IMD.

## Requerimentos

### Tarefas

[PIC2](task-pic-2)

### Outros

Conhecimento sobre o que é um sinal de PWM e o duty cycle.

## Duração programada

Cerca de 7 dias.

## Objetivos

+ Saber o que é o IMD, o que faz no carro e onde está ligado.
+ Criar código capaz de determinar a frequência e duty cycle de um sinal de PWM utilizando um PIC.
+ Extrair informação do sinal de PWM do IMD que codifica a informação variando a frequência e o duty cycle do sinal.

## Descrição

O IMD (Insulation Monitoring Device) é um aparelho requerido por regulamento (secção EV 7.4) que determina se existe ou não uma quebra de isolamento entre a alta e baixa tensão do carro. O modelo de IMD requerido por regulamento é o IR155-3204. Este aparelho para além de um sinal binário (IMD_OK) que diz se existe um problema de isolamento ou não também faz output de um sinal de PWM que transmite outro tipo de informações. O regulamento não obriga a que o sinal de PWM seja lido, mas o mesmo transmite um conjunto de informações interessantes (estado, resistência de isolamento, falha na ligação, etc).

Neste momento este output do IMD encontra-se ligado a PIC mas não existe código para ler o sinal e retirar de lá alguma informação interessante.

A parte dificil da tarefa pode ser geralmente definida como medição da frequência e duty cycle de um sinal utilizando um PIC. Esta parte é independente do facto de estarmos a tentar tirar informação do IMD e tanto quanto se sabe nunca foi feito nada parecido no projeto. O recurso principal será portanto o Google.

A restante parte da tarefa requer apenas tranduzir a medição de frequência e duty cycle do sinal para retirar a informação disponibilizada pelo IMD. O recurso aqui será apenas a datasheet do IMD que se encontra nos recursos.


## Recursos

### Websites

+ [Google](http://google.com)

### Documentação

+ Datasheet do IMD 
[IR155-32xx-V004_D00115_D_XXEN.pdf](/uploads/6765c9ae47b4ece8936c7bc793fb818a/IR155-32xx-V004_D00115_D_XXEN.pdf)