# [EL1] Tarefa de Eletrónica 1

## Introdução


As competições de Formula Student seguem um regulamento detalhado e apertado, com grande ênfase na electrónica do carro. Uma das regras mais importantes e consequentemente sistematicamente verificada é a regra que diz respeito à plausibilidade entre sensores de pedal ([FSG Rules 2018](https://www.formulastudent.de/fileadmin/user_upload/all/2018/rules/FS-Rules_2018_V1.1.pdf) - T10.3)

## Regulamento

Referente à plausibilidade entre os sensores do pedal do acelerador (**A**ccelerator **P**edal **P**osition **S**ensor), o regulamento refere que:
* **At least two separate sensors must be used as APPSs.** Separate is defined as not sharing supply or signal lines.

* **If an implausibility occurs between the values of the APPSs and persists for more than 100 ms the power to the motor(s) must be immediately shut down completely.** It is not necessary to completely deactivate the tractive system, the motor controller(s) shutting down the power to the motor(s) is sufficient.

* **Implausibility is defined as a deviation of more than ten percentage points pedal travel between any of the used APPSs** or any failure according to T 10.4.

## Objetivo 

O objectivo deste desafio é a implementação de uma solução que faça a verificação dos dois sinais dos sensores e actue o output conforme regulamento, ou seja, sempre que os dois sinais dos sensores divergirem mais que 10%, o sinal para os motores tem de ser nulo.

### Inputs

* **Sensor 1**- 0 a 5 V; Traduz a posição do sensor 1 do pedal de forma linear 
<br/>(0V -pedal solto; 5V - pedal em fim de curso).
* **Sensor 2**- 0 a 5 V; Traduz a posição do sensor 1 do pedal de forma linear 
<br/>(0V -pedal solto; 5V - pedal em fim de curso).

* Sinal 5V
* Single GND (0V)

## Output

Sinal de controlo dos motores (0 V - binário nulo; 5V - binário máximo).


## Entrega

Pretende-se uma proposta para a implementação de um sistema que cumpra o objectivo. O sistema pode fazer uso de componentes analógicos e/ou digitais, não havendo restrições para além do cumprimento de regulamento e de boas práticas de engenharia. Devem apresentar tudo o que for necessário para a implementação, desde esquemáticos a especificações de componentes. 
Aconselha-se ainda o uso de software para teste de circuitos como LTSpice ou PSpice.

## Material

* [**Divisores Resistivos**](https://www.allaboutcircuits.com/textbook/direct-current/chpt-6/voltage-divider-circuits/)

* [**Portas Lógicas**](http://www.electronics-tutorials.ws/category/logic)

* [**Comparadores**](http://www.electronics-tutorials.ws/opamp/op-amp-comparator.html)

(estes links são apenas sugestões, aconselho a pesquisarem noutras fontes estas *keywords*)