# [BSPD1] Testing the BSPD

## Requerimentos

### Tarefas

[ELG](task-el-g)

## Duração programada

7 dias

## Objetivos

Criar uma caixinha para testar o BSPD. Para se poder testar o BSPD no scrutineering e cá de forma fácil sem usar uma fonte.

## Descrição

### Conhecer o BSPD e como é testado

Antes de fazer esta tarefa é bom conhecer o BSPD. Existem várias regras de regulamento que estão relacionadas com o BSPD e existe no nosso ESF uma secção a falar sobre o BSPD.

No regulamento existem também regras sobre como o BSPD tem de ser testado.

Testar o BSPD involve conhecer também como funciona o sensor de corrente  utilizado (HTFS 200-P).

### Cirar uma caixinha para testar o BSPD



## Recursos

### Documentação

+ [ESF](https://drive.google.com/open?id=1KQwbIcim3UNWR7ZXVBzF6J4RvfB0kH3g)

### Datasheets

+ [Datasheet HTFS 200-P](https://www.lem.com/docs/products/htfs_200_800-p_sp2.pdf)

### Outros recursos

[Google](www.google.com)