# [BAT1] Energy Storage

## Requirements

### Técnico

None

### Tasks

None

## Objectives

After completing this task you should:

+ Have an idea of the forms of energy storage that exist.
+ Understand the parameters used to characterize an energy storage solution.
+ Know the principals, advantages and disadvantages of each form of **electric** energy storage.
+ Justify why we use batteries.

## Programmed Duration

Fast introductory research. Should take only a couple of hours.

## Description

### Introduction

There are numerous ways of storing energy. We are obviously interested in storing electric energy. From the available storage solutions we opt for lithium batteries. You should know why.

### Research

You should start by a quick research on energy storage methods. Mechanical, electromagnetic, electrochemical, thermal, ... You should understand their working principals.

You should understand the most important parameters of an energy storage solution. For example storage capacity, energy density and power density.

You should now look deeper into ways of storing electric energy. You should at least know the about capacitors, supercapacitors and batteries. You should know their working principals, defining characteristics and application.

Look at the rules about accumulators. 
Compare each possible solution, it's advantages and disadvantages and reach a conclusion of what technology is the best for our application.

Your final goal is to justify why we use lithium batteries and not, for example, supercapacitors.

## Material

+ Wikipedia and simple google search is enough.
