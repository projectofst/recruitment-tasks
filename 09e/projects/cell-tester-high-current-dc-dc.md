# Cell Tester - High current DC-DC

## Description

To test the characteristics of a lithium cell high charge and discharge currents (more than 100 A) are needed. Also this current must be controllable. For this a DCDC converter may be implemented usually using a PWM controller and choosing the components around that controller.

## Objectives
* Develop a solution capable of applying varying charge and discharge currents to a lithium cell in a safe way.
* Replace the Cell Charger's power circuit with something more capable.

## Requirements
 * 

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents
