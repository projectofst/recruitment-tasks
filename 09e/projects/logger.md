# Logger

## Description

The logger project aims to replace the current logger from kvaser with a self
built system.

The logger is a system capable of registrying CAN data to non volatile memory. 
It should also convert CAN data into a computer readable protocol (like USB).

## Objectives

* Develop a logging solution for CAN lines.

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents
[kavser memorator v2](https://www.kvaser.com/product/kvaser-memorator-pro-2xhs-v2/)