# Wireless sensor

## Description

The wireless sensor project target is to design a temperature and pressure
sensor capable of transmiting data over a wireless medium.

## Objectives

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents
