# Benchtop Signal Generator

## Description

Usually when testing a PCB multiple power supply voltage and digital and analog signals are required. To generate all this sometimes multiple power supplies and signals generators are necessary which ocupy a lot of space and may be scarce.

## Objectives
* Develop a solution to make testing PCB's easier.
* Replace multiple power supplies and signal generators with a small and practical module.

## Requirements
 * Input 230 V AC.
 * Power supply output: 1 x 24V, 1 x 5V, 1 x 3.3V.
 * Digital signal output: 2 to 5 outputs with different voltages (3.3V, 5V and 24V). Also with high impedance state.
 * Analog signal output: 2 to 5 outputs for different ranges (0-3.3V, 0-5V).
 * Digital control of the outputs using a computer interface.

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents