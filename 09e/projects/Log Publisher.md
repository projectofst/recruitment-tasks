# Log Publisher

## Description

FST08e currently outputs 150kb/s of data through its 2 CAN buses from various devices on the car.
This data is stored using a [Kvaser Memorator Pro 2xHS v2](http://canlandbucket.s3-website-eu-west-1.amazonaws.com/productionResourcesFiles/03725b18-9d2b-4672-8c68-36efe2440eba/Kvaser%20Memorator%20Pro%202xHS%20v2%20-%20A4%20-%20Web.pdf) in an SD Card.

Every time we test the car we store the data generated though the test in Google Drive in a csv format. A 30min run of the car generates aroung 40Mib of data.
We aim to replace Google Drive with a database to increase the usefullness of the data.

Your task is to send this data to a web server that will store the data in a database. This functionality should be integrated into our FST CAN Interface for seamless usage during the tests.

## Objectives
Deliver car data to a webserver
Integrate this in the FST CAN Interface

