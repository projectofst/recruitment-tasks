# Cell Tester - Energy Meter

## Description

To test a cell's characteristics a good measurement system is needed that is capable of measuring accurate data for posterior analysis. Relevant data includes voltage, current and temperature.
The goal of this project  is to accurately measure temperature, current and voltage values of the battery cell in order to provide better understanding of the cell's characteristics. This values also need to be available for CAN.
## Objectives
* Develop a measurement solution for cell testing. Capable of measuring voltage, current and temperature accuratly.
* Finding new tools that enable us to measure different things simultaneously
* Replace the Cell Charger's measuring circuit for soomething simpler with the single function of measuring.

## Requirements
 * 

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents