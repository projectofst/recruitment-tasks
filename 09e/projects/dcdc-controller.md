# DCDC controller

## Description

The DCDC controller aims to replace the current DCDC converter used in FST09e
with a more advanced, configurable and reliable system.

## Objectives


## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents

