# Cell Tester - Single cell BMS

## Description

When a cell is to be subjected to any kind of charge and discharge precautions are needed to ensure the cell is operated within safe limits. The job of a BMS is to monitor and protect a battery so that it is kept within its limits.

## Objectives
* Develop a small BMS for a single cell to ensure safe operation of the cell.
* Replace the Cell Charger's monitoring circuit for soomething simpler with the single function of protecting the cell.

## Requirements
 * 

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents