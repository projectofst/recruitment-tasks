# CAN Database

## Description
In order to save and access CAN messages, data must be organized in a database.

## Objectives
* Save all aquired CAN messages
* Messages must be accessible via internet
* Querys:
    + Time
    + Location
    + Car
    + CAN id
    + Type of run
* Access must be managed via FST interface

## Submissions
* Stage 1
    + Simple database
    + Time query
    + Interface widget for pushing and pulling
    + Submissions:
        * Database Schema
        * Test Procedure
    
* Stage 2
    + Database includes log metadata
    + Querys for metadata
    + CAN id query
    + Interface widget updated for querys
    + Submissions:
        * Database Schema
        * Test Procedure

* Stage 3
    + WWW access
    + Interface widget updated for www access
    + Submissions:
        * Database Schema
        * Test Procedure