# Cooling system control

## Description

The cooling system control project target is to design a system capable of
monitoring, powering and controlling the cooling system.

## Objectives
* Replace the current relay based soluction for power control
* Monitor the cooling system state: fan speed, pump speed, water temperature,
  motor temperature, inverter temperature, ..
* Control the cooling system state.

## Requirements
 * Able to drive a pump with 5A at 24V
 * Able to drive a fan with 20 to 100% of the top speed
 * Toggle pump via serial communication
 * Capable of measuring 6 temperature sensors with an accuracy of 1°C
 * Weight less than 500 g.
 * PCB area smaller than 100x100mm

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents
[Refrigeração do propulsor elétrico de um veículo Formula Student - Pedro Fontes](https://drive.google.com/file/d/12s0k45kbq0XhlMp-Z_G2eadepB0pdWuY/view?usp=sharing)
