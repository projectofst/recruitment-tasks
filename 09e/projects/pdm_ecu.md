# PDM ECU

## Description

The power distribution manager project aims to replace the current Digital
Control Unit on FST09e with a more advanced, configurable and reliable system.

## Objectives
* Replace the current relay based soluction for output control
* Replace the current fuse based soluction for output protection
* Monitor the output voltage and current
* Monitor any other needed paramenters
* Report the state of the outputs
* Report the fault conditions of the outputs

To complete this project a printed circuit board should be designed,
microcontroller code and a computer interface should be written.

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents

[FSAE PDU](https://digitalcommons.calpoly.edu/cgi/viewcontent.cgi?referer=https://www.google.com/&httpsredir=1&article=1415&context=eesp)
