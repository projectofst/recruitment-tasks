# Headlight

This project goal is to develop a headlight system for a FST car.

## Description
The project consists on:
- Selection of the lights to be used;
- Desing and implementation of the power system for the lights;
- Selection of a photoresistor to be used to control the lights;
- Communications protocol decision and implementation;
- Design and production of a PCB for control of the headlights;
- Programming the desired control system and communication method;
- Validation and documention.

## Objectives
* Creation of the first iteration of removable headlights for the car.
* Creation of an headlight that won't turn in a bright environment, and when in a dark environment it might be turned of by the pilot.

## Submissions
* Light selection - proposal and justification;
* Power distribution design;
* Photoresistor selection - proposal and justification;
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code tested
* Computer interface
* Testing of the whole system;
* Documentation of the project in parallel to it's development.

## Reference documents
[LED Drivers](http://www.ti.com/lit/an/slyt084/slyt084.pdf)
[Spain acceleration](https://drive.google.com/open?id=1IP9cv18vvKBOR-bBLPoRl02azdF0-fhv)

