# [ELG] Eletro Geral

## Introdução

O objetivo desta tarefa é a familiarização com o funcionamento básico de um carro de Formula Student, mais especificamente um carro da equipa.

## Tópicos a desenvolver

### Powertrain - O caminho da potência do carro (desde a bateria até à roda)

+ Falar sobre os seguintes elementos e como se interligam uns com os outros:
    - Bateria (Função no contexto geral e especificações básicas)
    - Inversores (Função e especificações básicas)
    - Motores (Função e especificações básicas)
    - Transmissão (Função)
    - Roda

### Pedal Signal Path

+ Falar dos seguintes tópicos:
    - Tipo de sensor usado para adquirir o input do piloto (adquirido pelo Torque Encoder).
    - Utilização do pedal (inversores).

### Medidas de Segurança/Indicação

+ Falar dos seguintes elementos de segurança/indicação
    - Shutdown Circuit e AIR (Funções e que é um AIR)
    - Master Switches (Função de cada um)
    - BSPD (Função)
    - TSAL (Função)
    - Brake Light (Função)
    - RTDS (Função)
    - IMD (Função)
    - BOTS (Função)
    - Dash (Quais os LEDs presentes e a sua função)
    - HVD (Função)
    - Precarga e Discarga do circuito intermédio (Função)
    

## Entrega

Devem ser capazes de explicar todos os conceitos descritos acima, através de uma pequena apresentação.

Maior parte daquilo que é necessário para saber o que é cada uma das coisas referidas encontra-se no [regulamento atual da FSG](https://www.formulastudent.de/fileadmin/user_upload/all/2018/rules/FS-Rules_2018_V1.1.pdf). No entanto o regulamento é muito overwhelming e a melhor maneira de perceberem a estrutura geral do carro é falarem com as pessoas em eletro. Outra maneira bastante boa é ler o  [ESF](https://drive.google.com/open?id=1KQwbIcim3UNWR7ZXVBzF6J4RvfB0kH3g)





