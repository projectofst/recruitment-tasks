# DC-DC Controller

## Description
Currently, the DC-DC converters used in the car aren’t monitored. These converters are small and, being placed inside the accumulator enclosure, heat up a lot. My project aims to create a circuit that has the ability to monitor input and output voltages and currents of the converters, monitor each converter’s temperature and activate or deactivate each converter’s output (allowing the team to activate and deactivate each converter individually and giving the converters a thermal protection). The acquired data is relevant to determine how much energy the converters use to maintain the LVS running (and their effect on the battery charge), converter’s efficiency, power consumed by the LVS among other possibilities. The acquired data should be converted to a CAN message.
## Objectives

 - Improve the current DC-DC solution.
 - Monitor sensor data – Voltage, current and temperature.
 - Switch the DC-DC converter’s output ON and OFF from user input or
   from thermal protection circuit.
 - Convert the data to a CAN message.

## Requirements
 - Accumulator side voltage and current sensors (isolated from the LVS).
 - Output voltage and current sensors.
 - Switch for the converter’s output (must be able to handle the
   necessary currents for the LVS without heating up too much and without damaging the switch).
 - Temperature Sensors.
 - Receive commands as input (CAN).
 - Send acquired information through CAN.


## Scheduling
 - Requirements - Tuesday (22nd Jan) (done)
 - Basic Implementation for each functionality - 1 week (done)
 - Parts choosing for each implementation (Suitable ICs, Resistor Power Ratings, Voltage Ratings, Resistor/Capacitor tolerances, etc) - 1 week (done)
 - Connecting all Implementations (in blocks) - 1 week 
 - Schematic (Altium) - 1 week
 - Layout (Altium) - 2 weeks
 - Production - 4 weeks
 -  Code (during production)
 - Assembly
 - Testing
 - Documentation



## Implementation

Excluding the 2Ohm Power Resistor, all the chosen resistors belong to the E24 series.

### Temperature Monitoring
The temperature monitoring will be done from the LV circuit. I chose to use glued NTCs on the surface of the converters or on their heatsinks. The NTCs will be placed on a voltage divider with a buffer. (The voltage divider should be fed with 3.3V).

![alt text](https://arduinodiy.files.wordpress.com/2015/11/spanningsdeler2.jpg?w=840 "NTC Voltage Divider")

Vo is connected to a buffer, whose output connects to a pin of the PIC. The PIC will handle the acquired values from the voltage divider and calculate the temperature.

I chose a 10k NTC (NTCLE100E3 from Vishay) which has B = 3977 K. The PIC will calculate the temperature using the following formula:

![alt text](https://arduinodiy.files.wordpress.com/2015/11/codecogseqn9.gif "NTC Temperature Calculation")

The power handled by the pull-up resistor is 272.25uW at 25ºC so a common 0603 SMD resistor should be enough.



### LV side Voltage Monitoring
The Voltage on the LV side of the converters will be monitored through a Voltage Divider. The reading will be made through a buffer connected to one pin of the PIC, in order to keep the correct functioning of the voltage divider.
The maximum voltage on the output of the converters is 26.4 V. The Voltage Divider must be designed in a way that makes the PIC handle a maximum of 3.3V when the output of the converters is 26.4V.

![alt text](http://eagerlearning.org/wp-content/uploads/2014/01/VoltageDivider.png "Voltage Divider")

I chose R1 = 91k and R2 = 13k

The power handled by R1 is 5.87mW and the power handled by R2 is 838.8uW. 0603 SMD resistors should be enough.

### LV side Current Monitoring
For the LV side current monitoring, I chose a current transducer (HX 10-NP/SP3 from LEM), in a parallel configuration in order to handle 20 A.
For positive currents, the maximum (Vout-Vref) is 0.625 (at 20 A). To get an accurate reading through the PIC, this voltage must be amplified 5.28 times. For that, I went with an AI (MCP6N11 from Microchip), choosing Rf = 4.3k and Rg = 1k.

Rf and Rg should be 0603 SMD resistors.

### LV Switches
In order to switch ON and OFF the converter's outputs, I decided to use N channel MOSFETs. I chose to use PSMN1R5-30YLC (from NEXPERIA) since it has a very low ON resistance (1.55 mohm). At a maximum of 15 A, the power loss is only 348.75 mW. However, this is only true if Vgs=10V and the PIC supplies only 3.3V. According to the datasheet, at 4.5V, Rdson = 2.05 mOhms, which means a power loss of 461.25 mW at 15 A, so I will use two MOSFETs in parallel for each switch, lowering the power loss.
The MOSFETs will disconnect the negative terminal of the converters.

In order to keep the MOSFET's Rdson as low as possible, I'll be using a voltage regulator (NJM2386/88 from JRC) to step down 24V to 12V and two TC4420 Gate driver ICs (from Microchip) - one for each LV switch - to fully turn on the transistors.

MOSFET Datasheet: https://pt.mouser.com/datasheet/2/916/PSMN1R5-30YLC-1479841.pdf

Linear Voltage Regulator: https://pt.mouser.com/datasheet/2/294/NJM2386_NJM2388_E-45201.pdf

TC4420: https://pt.mouser.com/datasheet/2/268/21419c-23818.pdf

I chose this regulator because it can handle up to 35V.

![alt text](https://i.imgur.com/m9awhyF.jpg "Gate Driver Circuit")

The 100 ohm resistor will be replaced by a 49.9ohm resistor. The gate resistors will handle significant peak power - 1206 SMD resistor.

### HV side Voltage Monitoring
I decided to use the solution implemented in the HV monitor, changing only some resistor values. The voltage divider on the input should be modified, in order to step down 300V to 2V instead of 600V (The two 3.01MOhms series resistors - R9 and R11 -  should be replaced by a 1.5MOhm Resistor and R12 should be 10kOhms). R13 should be 13k and R14 should be 20k.
![alt text](https://i.imgur.com/x0axfeS.png "HV Sensor")

R13 and R14 should be 0603 SMD resistors.
The 1.5MOhm resistor (replacing R9 and R11) and R12 must handle significant Voltages since they will create a voltage divider from 300V to 2V. For that, I'll be using High Voltage Resistance Chip Resistors.

High Voltage Resistance Chip Resistors Datasheet: https://pt.mouser.com/datasheet/2/348/ktr-e-1139187.pdf


### HV side Current Monitoring
To measure the current on the HV side, I decided to use a shunt resistor. I chose to use the circuit used for the HV side Voltage Monitoring, with a few changes: Instead of measuring the voltage between the accumulator terminals, it will measure the voltage between the two terminals of a Resistor.

![alt text](https://i.imgur.com/QwNrTsf.jpg "HV Current Sensor")

DCDC2 is a RKE-2405S/H

Resistor should be 2Ohm 5W.

### CAN
The user input must be recieved through CAN and all the acquired data (input voltage, current and power on both converters, temperature and output voltage, current and power on both converters) must be sent through CAN.
I'll use a transceiver - MCP2562T-E/SN from Mircochip.

This project's PIC will be connected to an existing CAN bus.

![alt text](https://i.imgur.com/89sRjV6.png "Transceiver Connections")


Transceiver Datasheet: https://pt.mouser.com/datasheet/2/268/20005167C-1512552.pdf

### PIC
I'll be using a 3.3V PIC - dspic33ep256mu806.