## DESCRIÇÃO

   A solução de refrigeração do FST-08e e a que está planeada para o FST-09e usam uma
   potência constante para o sistema de refrigeração. O objectivo deste projecto
   (nome de código: Sub-Zero) é a criação de um PCB capaz de receber informação dos
   sensores de temperatura e caudal do carro e aumentar ou diminuir a potência
   fornecida a estes conforme as necessidades em tempo-real dos diversos componentes
   monitorizados, o que irá, previsivelmente, aumentar a eficiência energética do carro. 


## OBJECTIVOS

    • Substituir a actual solução com base em relés
    • Monitorizar o estado do sistema de refrigeração:
         ◦ velocidade da ventoinha
         ◦ velocidade da bomba
         ◦ temperatura da água
         ◦ temperatura do motor
         ◦ temperatura dos inversores
    • Controlar o estado de refrigeração
    • Transferência das temperaturas medidas através de CAN para o Data Logger
    • Controlo da temperatura através de uma temperatura de referência

## REQUISITOS

    • Alimentar bombas e ventoinhas com 5A a 24V
    • Capaz de medir até 8 sensores de temperatura sem acrescentar um erro de medição superior a 1ºC
    • Capaz de medir temperatura com uma resolução de 0,1ºC e um range de 0ºC a 85ºC
    • Capaz de medir a velocidade de bombas através de sensores de caudal ou de tacómetro
    • Capaz de controlar bombas e ventoinhas através de output PWM ou potência
    • Pese menos de 500g
    • Tenha uma área inferior a 100x100mm
    • Tenha 8 inputs para termístores
    • Tenha 3 inputs para tacómetros
    • Tenha 2 inputs para sensores de caudal
    • Tenha 3 outputs de potência
    • Tenha 3 outputs de PWM
    • Tenha output de CAN