PESQUISA
========

    Pesquisar e escrever relatórios sobre os seguinte temas necessários para a concretização da PCB

Temperature Measure
--------
1 semana

Flow Measure
-------
1 semana

Motor Control
---------
1 semana

Power
------
1 semana

MCU/prog
---------
1 semana

Arquitectura
=========

    Conceber a arquitectura da PCB

1 semana

Schematic
=========

    Criar o esquemático do circuito da PCB

2 semanas

Layout
========

    Criar o layout da PCB para produção

2 semanas

Produção
=========

    Time window em que a PCB deverá estar pronta para produção
    
4 semanas

Programação
========
    
    Programação do PIC
    
4 semanas

Assembly
========

    Soldadura dos componentes à PCB

1 semana

Testing
=========

    Testes e tweaks do funcionamento da PCB

1 semana

Documentação Final
========

    Finalização da documentação do código e funcionamento da PCB

1 semana

Closing
=========

    Tweaks finais e finalização do projecto
    
2 semanas