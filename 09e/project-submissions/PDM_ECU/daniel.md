## PDM ECU
## Description
This project consists of the development of a new control unit for the power distribution manager, to replace the old outdated one. This control unit should be supplied with LV dc, and redirect this current to the other eletric systems connected to this module, monotoring the output signal, and closing it in case of an anomaly with that system.
## Objectives
* Replace the current relay based solution for output control;
* Replace the current fuse based solution for output protection;
* Monitor the output voltage and current and any other needed parameters;
* Report the state of the outputs and fault conditions of the outputs through the CAN bus;
* The output conditions must be configurable from the CAN bus.
## Requirements 
* 1 input 24 V dc;
* 1 input GND;
* 3 "high" current outputs with 24 V dc, ranging from 10 to 20 A;
* 10 "low" current outputs with 24 V dc, ranging from 1 to 10 A;
* 1 input/output to the CAN bus.
## Submissions 
* Eletronics architecture - 22/01 to 12/02 
    * Current and voltage measure modules - 22/01 to 29/01
    * Switch module - 29/01 to 5/02
    * Power, communication and MC modules - 5/02 to 12/02
* Eletronics schematic - 12/02 to 26/02
* Eletronics layout and PCBs production - 26/02 to 8/03
* Embedded code - 8/03 to 22/03
* Eletronics assembly and test procedure - yet to be defined
* Documentation - yet to be defined
