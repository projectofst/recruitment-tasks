## Dashboard

#### Description
The aim of this project is to develop a dashboard for the car capable of showing all the information that the driver will need while driving. The application will be done in QML and C++ using Qt.

#### Objectives
- Be configurable
- Receive data through the CANmessage struct like the interface's widgets
- Adapt the existing message generator to produce a structured sequence of messages for testing purposes (e.g. going from 0 to 100 km/h)
