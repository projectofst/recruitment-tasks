# Benchtop Signal Generator

## Abstract
This project aims to develop a Signal generator for circuit logic test. 


## Description
 The development of PCB's boards for FST Lisboa goes through a testing phase. 
 During this phase the circuits are subjected to electrical conditions equivalent to those present in the competition car.
 Usually when testing PCB multiple power supply voltage and digital and analog signals are required. 
 Some of these digital signals follow non-standard voltages.
 
 One of the current challenges during this phase is the need to use several power supplies and signals generators which ocupy a lot of space and may be scarce.
 
 This project aims to solve this problem by introducing manual and automatic ways of controling the generation of this signals in a small board.

## Objectives
  
### Main objectives:

* Develop a solution to make testing PCB's easier.
* Replace multiple power supplies and signal generators with a small and practical module.
### Other: 
* Compatibility with non-common signals and future proof.
* Develop a Computer program test suit for automation of tests. (file with temporary and voltage indications makes the necessary changes)
* Protected outputs and inputs.
 
## Requirements
 * Input 230 V AC.
 * Power supply output: 1 x 24V, 1 x 5V, 1 x 3.3V.
 * Digital signal output: 2 to 5 outputs with different voltages (3.3V, 5V and 24V). Also with high impedance state.
 * Analog signal output: 2 to 5 outputs for different ranges (0-3.3V, 0-5V).
 * Digital control of the outputs using a computer interface (via CAN comunication).
 * Send acquired information to computer interface (via CAN comunication)
 * Manual Control of digital outputs and at least one analog output.

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Project phases and delivery:
	1- Close project Requirents  [Done]
	2- Source Components and budget analasys. (1 week)
	3- Development of main blocks (1 week)
	4- aggregation(1 week)
	5- Simulations (1/2 week)
	6- 1ª Delivery: Main Squematic in paper.
	7- 2nd Delivery: Altium esquematic (1 week)
	8- Pcb Layout  (2 weeks)
	9- 3rd Delivery: BOM and Gerber Assembly 
	10- Production + Coding(4 weeks)
	11- Assembly (1 week)
	12- Tests (1 week)
	13- Report(1/2 week)
    14-Project Closure


Project Calendar:

![alt text](https://ruaseu.000webhostapp.com/wp-content/uploads/2019/01/A-e1548171073983.png "A")

![alt text](https://ruaseu.000webhostapp.com/wp-content/uploads/2019/01/B-e1548171151347.png "B")

![alt text](https://ruaseu.000webhostapp.com/wp-content/uploads/2019/01/C-e1548171170337.png "C")




Author:
João Ruas
joao.pato.ruas@tecnico.ulisboa.pt