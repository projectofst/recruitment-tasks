# Bluetooth Module Report

# Introduction

In order to make this project possible, a bluetooth module must be chosen. For this, 6 different bluetooth modules were compared, being evaluated in 10 different aspects:

![Bluetooth Module](bluetooth_comparison.png)

Based on this table, the elected module was the MicroCHip's RN4871. Although this module is very similar to RN4870, RN4871 is smaller and cheaper.

# Device

RN4871 has the bluetooth Version 4.2 BLE, wich is great for self power aplications, and has a 3.3V typical operating voltage. This module has the ability to operate without a MCU in simple applications, such as, temperature and pressure sensors due to an existing non volatile memory, a running scrypt and programmable analog inputs. More informations can be seen in the [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/50002489C.pdf) and in the [User's Guide](http://ww1.microchip.com/downloads/en/DeviceDoc/RN4870-71-Bluetooth-Low-Energy-Module-User-Guide-DS50002466C.pdf).

# Procedure

The module's settings that need to be changed/consulted will be changed connecting it to a PC and using a terminal emulator like CoolTerm. Some settings of the module, like name and MAC adress, will be consulted before connecting the system. 3 different PIO pins will be configured for monitoring the module while it's working and it will be used a UART to usb converter so that the conection to the PC is possible. Every necessary command can be seen in the [user's guide](http://ww1.microchip.com/downloads/en/DeviceDoc/50002466B.pdf). A eletronic conection example can be found in figure 5-4 of the [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/50002489C.pdf). This module can be easily tested with [RN4871 PicTail](https://www.microchip.com/developmenttools/ProductDetails/RN-4871-PICTAIL) or something similar.

## Transmitter
    
The transmitter will work without a MCU due to it's simple application. A sensor will be conected to one of the analog pins in the module and a 3.3V cell will provide the voltage onto the system, the cell capacity will be calculated with the table 2-4 in the [datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/50002489C.pdf). A scrypt will be used to enter the module's command mode, connect to the receiver via MAC adress and establish a bond conection, then the module will enter data mode and the data from the analog pin will be acquire and sent trough UART to the receiver module. Cell conection example:
![Cell connection](cell_example.png)

## Receiver

The receiver will work with a MCU due to CAN compatibility. The module will be connected with the MCU via UART and 24V will be provided, so it'll be needed a DCDC 24-3.3V converter. The module will wait until the transmitter establish the conection, receive the data and send it to the MCU, then the MCU will convert the data to CAN message and send it to the CAN bus. Power conection example:
![24V connection](power_example.png)