# Wireless Sensor Project

# Description

The target of this project is to design a sensor capable of transmitting data via bluetooth. For this 2 PCBs will be built, one will be the receiver PCB and the other will be the transmitter PCB, these PCBs will make all the necessary connections between the modules. The transmitter PCB will send the sensor data to the receiver PCB and the receiver will convert it to CAN.

# Objectives

* Replace the current solution for obtaining sensors data.
* Monitor de sensor data, such as, temperature or pressure.
* Transmit the sensor data between the transmitter and the receiver.
* Make the receiver send a CAN message with the sensor data.

# Requirements

* Sensor.
* Way to acquire sensor data.
* Way to send/receive sensor data trough bluetooth.
* Way to convert sensor data to CAN
* 24V alimentation on the receiver end.
* Self power on the transmitter end.

# Schedule

* Solution search - until 22-Jan-2019
* System Architeture and Components Design - 23-Jan-2019 to 17-Feb-2019
* PCB schematic - 17-Feb-2019 to 24-Feb-2019
* PCB layout - 24-Feb-2019 to 3-Mar-2019
* PCB production - 3-Mar-2019 to 1-Apr-2019
* Software Develop - while production of the PCB
* PCB assembly - 1-Apr-2019 to 8-Apr-2019
* System testing - 8-Apr-2019 to 20-Apr-2019
* Documentation - 20-Apr-2019 to 27-Apr-2019
* Finale - 27-Apr-2019
