# Cell Tester - Single Cell BMS

# Description
The goal of this project is to design and develop a battery management system capable of not only regulate the charge and discharge levels of a single LiPo cell,
but also to send, via CAN, information about the cell voltage and temperature.
It will be connected to a MCU which will translate the information received by the BMS to a CAN message. This will then be translated by another MCU to USB, so it can be readable
on the interface.

# Objectives
* Disconnect the cell from the load if the charge voltage exceeds 4.2V or if it decreases from 3.0V, or if it reaches a high temperature level
* Send temperature and voltage levels across CAN

# Requirements
* Weigh less than 500g
* PCB area must be smaller than 100x100mm
* Must have a CAN output

# Scheduling
* Requirements (22nd Jan)
* Basic Circuitry and Components (~5th Feb)
* Connecting the blocks and basic schematic (~12th Feb)
* Layout (~26th Feb)
* Production and Coding (~26 Mar)
* Assembly, Testing and Documentation (~5 Apr)

# Implementation

# Temperature Monitoring
The cell's temperature will be measured by a NTC Thermistor. The latter will be mounted near the cell, so it can measure its temperature more accurately. 
It will be mounted as following:

![Imgur](https://i.imgur.com/TXm97cv.png "Thermistor Circuit")

V0 will be connected to a MCU's pin, where a voltage level will be read and, via the formula shown below and since we know R1 (in this case 10kOhm),
the thermistor's resistance at that given moment can be calculated.

![Imgur](https://i.imgur.com/cht7JGa.png "Voltage Divider")

Knowing the thermistor's resistance we can now calculate its temperature. This is achieved by the formula shown below, where B = 3435 (specific for the NTC
that i chose, NTCG063JF103FTB, by TDK), T0 = 25ºC, R0 = 10kOhm, R = Resistance measured, T = Current Temperature.

![Imgur](https://i.imgur.com/vKmABJ5.png "NTC Formula")

Having in account that LiPo cells should operate between 45ºC and 60ºC, the MCU will disconnect the cell from the load if it exceeds or decreases from such values by operating a transistor.

# Voltage Monitoring
The cell's voltage will be displayed in the interface via the MCU, as shown:

![Imgur](https://i.imgur.com/rtrvKgg.png "PIC Voltage")

V1 and V2 will be connected to 2 separate pins of the MCU. By calculating the difference between them, we get the cell's actual voltage.

# BMS
The chip that will control  the overcharge and overdischarge levels will be FS312F-G, by Fortune Semicon (shown below).
These levels and other values can be seen below:

![Imgur](https://i.imgur.com/0zdFLon.png "FS312F-G Values")

![Imgur](https://i.imgur.com/PBtxDM8.png "FS312F-G")

Pin 1 will protect against overdischarge.
Pin 2 will protect against overcurrent and short circuit.
Pin 3 will protect against overcharge.
Pin 4 will not be used, but it's normally used to modify the delay time of the chip.
Pin 5 and 6 are supply pins.

The interior of the chip is as following:

![Imgur](https://i.imgur.com/i3oCr8I.png "FS312F-G Interior")

# Schematic

![Imgur](https://i.imgur.com/v5g4yFR.png "Schematic")

As explained before, V0, V1, V2 and V3 will connect to a MCU.
The VCC of the cell will provide supply to the FS312F-G via R2, and its GND will connect to the chip's GND. C1 will be connected between them to surpress disturbance.
The overdischarge and overcharge mechanisms will be connected to MOSFETs, which will be disconnected if the cell surpasses the values said above.

# Components' Datasheets

Thermistor's: https://pt.mouser.com/datasheet/2/400/pd_commercial_ntc-thermistor_ntcg_en-1290965.pdf