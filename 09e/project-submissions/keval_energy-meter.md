# Cell Tester - Energy Meter

## Description

To test a cell's characteristics a good measurement system is needed that is capable of measuring accurate data for posterior analysis. Relevant data includes voltage, current and temperature.
The goal of this project is to accurately measure temperature, current and voltage values so that the team can understand better the battery cell's condition. those values also need to be available for CAN.
## Objectives
* Develop a measurement solution for cell testing. Capable of measuring voltage, current and temperature accuratly.
* Find new tools that can measure different things simultaneously
* Replace the Cell Charger's measuring circuit for soomething simpler with the single function of measuring.

## Requirements
 * NTC- temperature sensors (0.1/0.5ºC)
 * current (5A-[ACS714](https://www.allegromicro.com/en/Products/Current-Sensor-ICs/Zero-To-Fifty-Amp-Integrated-Conductor-Sensor-ICs/ACS714.aspx) [datasheet](file:///C:/Users/keval/AppData/Local/Packages/Microsoft.MicrosoftEdge_8wekyb3d8bbwe/TempState/Downloads/ACS714-Datasheet%20(1).pdf) and voltage sensors (0.1V)

## Scheduling
1. work on the solution for each module(temperature, current and voltage)-1 week
2. working on interacting all modules-1 week
3. creating the schematics-2 weeks
4. think about the layout-1 week
5. PCB prodution and code preparation-4 weeks
6. working on the PCB-2 weeks
7. testing-1 week
8. documentation-2 weeks

## Submissions
* Eletronics architecture
* Eletronics schematic
* Eletronics layout
* Eletronics test procedure
* Embedded code architecture
* Embedded code 
* Embedded code tests
* Computer interface architecture
* Computer interface code
* Computer interface tests

## Reference documents