# Cell Emulator 

## Overview 

The slave PCB is responsible for measuring voltage and temperature for 14 cells. So that this can be tested, this values must be generated and controlled over CAN.

## Technical requirements

+ Generate one temperature reference
+ Generate 14 voltage references
