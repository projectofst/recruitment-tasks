# Button and Selector Testing

## Overview

The steering wheel sends a lot of information inputted by the pilot to the dash PCB. In order to verify that these inputs are being well dealt with they must all generated individually. The proposed project is the automation for that. This should also report by CAN.

## Technical Requirements

+ 3 selectors
+ 6 buttons
