# Volage and Current Measurement

## Overview

This project's objective is to measure the voltage and current measure of a PCB/pump/fan/whole car. This information should be displayed in an intuitive way. The information must also be sent through CAN.


## Technical Requirements

+ Voltage: 30 V
+ Current: 10 A
+ Power: 120 W


