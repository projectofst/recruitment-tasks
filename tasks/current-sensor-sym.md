# BSPD's current sensor tester

## Overview

In order to verify T 11.6.1 power's, a HTFS 200-P current transducer is used. In order to test the compliance with the rule, it is necessary to simulate that the power exceeds the value by mimicking the sensor. This should be controlled via CAN.

## Technical requirements

+ Simulate from -120 A to 120 A in the HTFS 200-P
