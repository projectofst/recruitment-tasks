# Recruitment for FST10e 

## Introduction

This file aims to describe the recruitment in the area of eletronics for the car
to be developed in 2020/2021. 

In this file you can find general information concerning the recruitment done in 
this area.


## Area's Involvement 

Each member of the area of eletronics that is projecting FST10e should take part 
in the recruitment process for the next car. 

The recruitment in this area will envolve the attribution of
individual projects. Each project will be supervised by a member of the
current team (of the area of eletronics). 

Additionally, there will be classes given
to the recruits with the objective of clarifying especific doubts and teaching them 
the fundamentals of what is done in the project (and what will be done during their
recruitment).

## Structure

The recruitment in the team is divided in 3 stages. 

### First Stage

The first stage is the first contact of the recruits with the team. 

If the recruit's choice is to proceed onto the recruitment in eletronics, he
proceeds to the second stage explicit below, exclusive to eletronics.

### Second Stage

#### Tasks


The first task presented (to all the recruits who choose this area of interest)
is to prepare a presentation/document explaining the eletrical fundamentals of 
a car like FST09e. This should include powertrain, pedal signal acquisition, 
shutdown circuit, sensors, can interface, can usage in the car, bms architecture, 
generic eletronics module architecture and harnessing in eletric cars. 

Later this task will be better explained and  presented. The recruits will, after 
preparing for a maximum of 2 weeks, present their work to a member of the area 
of eletronics, who should give helpful feedback and clarify and doubts that 
emerged to the recruit.

Recruits will be given access to 
a project bank, created by the team, from where they will choose a project 
to work on. 

The available projects can be found in the [projects](https://gitlab.com/projectofst/recruitment-tasks/tree/freitas-recruitmen-fst010e/projects) folder.


Each recruit will be supervised by a member of the team while developing the project. 

Project phases:
+ Drawing the Schematics of the circuit needed for a given project (fundamented
and explained);
+ Use Altium to draw the Schematics;
+ Use Altium to draw the Layout;
+ Programm a Microcontroller.



#### Classes

In parallel with the first task of the second stage of the recruitment, we will
give 2 classes to the recruits: dsPIC; Altium. 

The first one should explain the general functioning of a microcontroller like 
the ones we use in the car. 

The second class should explain how the Altium software works and give indications
and suggestions about working with it.

Each of these classes will be given by a different member of the eletronics area.

After the first task classes will be continued. In the [classes](https://gitlab.com/projectofst/Documentation/classes)
repository there can be found the topics to be talked about in classes during the
rest of the recruitment as well as when the classes will take place and who will 
give them.


## Third Stage

In the third and last stage, the recruits will be helping with wiring and/or 
soldering, being part of the cars assembly. 

The recruits might also researxh/develop projects useful for next years car and/or work on
improving/adapting the project they developed during recruitment's stage two. The
objective of these adaptations would be to create something useful for this years 
and/or next years car.


## Note

Projects are not vinculative to FST011e position in the eletronics team. 
Assigment of projects doesn't entitle its assignee to have the project included 
in FST011e. Members can be excluded despite concluding the project.

## Schedule overview
The scheduling of the EL recruitment can be found in future documentation releases/updates.
